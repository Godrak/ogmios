TOP_DIR=$(dirname $(readlink -f $0))
BUILD_FLAGS="-DURHO3D_ANGELSCRIPT=0 -DURHO3D_LUA=0 -DURHO3D_URHO2D=0 -DURHO3D_SAMPLES=0 -URHO3D_TOOLS=0 -DURHO3D_C++11=1 -DURHO3D_THREADING=1 "

mkdir Native

BUILD_TYPE=Debug

#build urho3D first in folder Urho3D/build
bash Urho3D/script/cmake_generic.sh Native/Urho3Dbuild ${BUILD_FLAGS} -DCMAKE_BUILD_TYPE=$BUILD_TYPE
cd $TOP_DIR/Native/Urho3Dbuild
make -j 3

cd $TOP_DIR
#now build the project
bash source/scripts/cmake_generic.sh $TOP_DIR/Native/build -DURHO3D_HOME=$TOP_DIR/Native/Urho3Dbuild -DCMAKE_BUILD_TYPE=$BUILD_TYPE
cd $TOP_DIR/Native/build
make -j 3

