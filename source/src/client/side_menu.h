/*
 * side_menu.h
 *
 *  Created on: Sep 18, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_UI_SIDE_MENU_H_
#define SOURCE_SRC_UI_SIDE_MENU_H_

#include "../UI/ui_base.h"
#include "client_events.h"

class ConnectionMenu: public UIBase {
URHO3D_OBJECT(ConnectionMenu, UIBase)
	LineEdit *name_;
	LineEdit *ip_;
	LineEdit *port_;
	Button *confirm_;

	explicit ConnectionMenu(Context *context) :
			UIBase(context) {
	}

	virtual String GetWindowName() {
		return "ConnectionMenu";
	}

	virtual void SetupWindowContent() {
		name_ = CreateFramedOption<LineEdit>(window_, "name");
		name_->SetMinWidth(13 * FONT_SIZE);
		name_->SetText("player");

		ip_ = CreateFramedOption<LineEdit>(window_, "IP");
		ip_->SetMinWidth(13 * FONT_SIZE);
		ip_->SetText("127.0.0.1");

		port_ = CreateFramedOption<LineEdit>(window_, "port");
		port_->SetMinWidth(6 * FONT_SIZE);
		port_->SetText("1234");

		confirm_ = CreateFramedText<Button>(window_, "Confirm");
		SubscribeToEvent(confirm_, E_RELEASED, URHO3D_HANDLER(ConnectionMenu, HandleConfirm));
	}

	void HandleConfirm(StringHash eventType, VariantMap &eventData) {
		auto name = name_->GetText();
		auto ip = ip_->GetText();
		auto port = ToInt(port_->GetText());
		SendEvent(CE_CONNECT, connect::NAME, name, connect::IP, ip, connect::PORT, port);
	}

};

class SideMenu: public UIBase {
URHO3D_OBJECT(SideMenu, UIBase)
	ConnectionMenu *connectionMenu_;

	explicit SideMenu(Context *context) :
			UIBase(context) {
	}

	virtual void Show(bool val) {
		FixLayout();
	}

	virtual void FixLayout() {
		auto graphics = GetSubsystem<Graphics>();
		auto size = graphics->GetSize();
		window_->SetPosition(0, 0);
	}

	virtual String GetWindowName() {
		return "Menu";
	}

	virtual void SetupWindowContent() {
		connectionMenu_ = GetParent()->CreateChild<ConnectionMenu>();
		connectionMenu_->Init();

		auto connectButton = CreateFramedText<Button>(window_, "Connect");
		SubscribeToEvent(connectButton, E_RELEASED, URHO3D_HANDLER(SideMenu, HandleShowConnectionMenu));
	}

	void HandleShowConnectionMenu(StringHash eventType, VariantMap &eventData) {
		connectionMenu_->Show(true);
	}

};

#endif /* SOURCE_SRC_UI_SIDE_MENU_H_ */
