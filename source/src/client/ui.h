/*
 * ui.h
 *
 *  Created on: Sep 18, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_CLIENT_UI_H_
#define SOURCE_SRC_CLIENT_UI_H_

#include "../includeUrho.h"
#include "../events.h"
#include "side_menu.h"
#include "../UI/ui_base.h"
#include "client_events.h"
#include "connection_keeper.h"
#include "../custom_key_map.h"

class UIController: public LogicComponent {
URHO3D_OBJECT(UIController, LogicComponent)
public:
	explicit UIController(Context *context) :
			LogicComponent(context), usedKeys_(GetKeyMapping()) {
		context->RegisterFactory<SideMenu>();
		context->RegisterFactory<ConnectionMenu>();

	}

	void Start() override {
		input_ = GetSubsystem<Input>();
		root_ = GetSubsystem<UI>()->GetRoot();

		auto cache = GetSubsystem<ResourceCache>();
		auto styleFile = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
		root_->SetDefaultStyle(styleFile);

		sideMenu_ = root_->CreateChild<SideMenu>();
		sideMenu_->Init();
		sideMenu_->Show(true);

	}

	void Update(float timeStep) {
		auto server = GetSubsystem<Network>()->GetServerConnection();
		auto keeper = GetComponent<ConnectionKeeper>();
		if (server && keeper) {
			Controls c { };
			PODVector<unsigned char> pressed;
			auto input = GetSubsystem<Input>();
			for (const auto &pair : usedKeys_) {
				if (input->GetKeyDown(pair.first_)) {
					pressed.Push(pair.second_);
				}
			}

			VariantMap data { };
			data.Populate(pressedKeys::KEYS, pressed, pressedKeys::PLAYERID, keeper->GetPlayerId());
			server->SendRemoteEvent(E_PRESSED_KEYS, false, data);
		}
	}

private:
	UIElement *root_;
	SideMenu *sideMenu_;
	Input *input_;

	const HashMap<Key, OKey> usedKeys_;

};

#endif /* SOURCE_SRC_CLIENT_UI_H_ */
