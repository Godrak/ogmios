/*
 * client_events.h
 *
 *  Created on: Sep 22, 2020
 *      Author: Pavel Mikus
 *		email: pavel.mikus.mail@seznam.cz
 */

#ifndef SOURCE_SRC_CLIENT_CLIENT_EVENTS_H_
#define SOURCE_SRC_CLIENT_CLIENT_EVENTS_H_

#include "../includeUrho.h"

URHO3D_EVENT(CE_CONNECT, connect) {
URHO3D_PARAM(NAME, name);
URHO3D_PARAM(IP, ip);
URHO3D_PARAM(PORT, port);
}

#endif /* SOURCE_SRC_CLIENT_CLIENT_EVENTS_H_ */
