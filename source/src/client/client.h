#pragma once

#include "../hencoop.h"


using namespace Urho3D;

class Client: public Hencoop {
URHO3D_OBJECT(Client, Hencoop)

public:
	explicit Client(Context* context);
	virtual void Start() override;

protected:
	void HandleKeyDown(StringHash eventType, VariantMap& eventData);

private:
	Scene* scene_;

};

URHO3D_DEFINE_APPLICATION_MAIN(Client);
