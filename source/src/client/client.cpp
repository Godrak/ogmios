#include "client.h"
#include "connection_keeper.h"
#include "ui.h"

Client::Client(Context *context) :
		Hencoop(context) {
	SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(Client, HandleKeyDown));

	context->RegisterFactory<ConnectionKeeper>();
	context->RegisterFactory<UIController>();
}

void Client::Start() {
	Hencoop::Start();
	scene_ = new Scene(context_);
	scene_->CreateComponent<CameraController>(CreateMode::LOCAL);
	scene_->CreateComponent<ConnectionKeeper>(CreateMode::LOCAL);

	auto ui = scene_->CreateComponent<UIController>(CreateMode::LOCAL);

}

void Client::HandleKeyDown(StringHash eventType, VariantMap &eventData) {
	using namespace KeyDown;

	auto *input = GetSubsystem<Input>();
	if (input->GetKeyDown(KEY_ESCAPE)) {
		engine_->Exit();
	}
}

