/*
 * connection_keeper.h
 *
 *  Created on: Sep 13, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_CLIENT_CONNECTION_KEEPER_H_
#define SOURCE_SRC_CLIENT_CONNECTION_KEEPER_H_

#include "../includeUrho.h"
#include "../events.h"
#include "client_events.h"

class ConnectionKeeper: public LogicComponent {
URHO3D_OBJECT(ConnectionKeeper, LogicComponent)
public:
	explicit ConnectionKeeper(Context *context) :
			LogicComponent(context) {
		SubscribeToEvent(E_SERVERDISCONNECTED, URHO3D_HANDLER(ConnectionKeeper, HandleDisonnect));
		SubscribeToEvent(E_CONNECTFAILED, URHO3D_HANDLER(ConnectionKeeper, HandleDisonnect));
		SubscribeToEvent(E_CONNECTIONINPROGRESS, URHO3D_HANDLER(ConnectionKeeper, HandleDisonnect));

		SubscribeToEvent(CE_CONNECT, URHO3D_HANDLER(ConnectionKeeper, HandleConnect));
	}

	void Start() override {
		network_ = GetSubsystem<Network>();
	}

	void HandleConnect(StringHash eventType, VariantMap &eventData) {
		SetServerAndName(eventData[connect::NAME].GetString(), eventData[connect::IP].GetString(), eventData[connect::PORT].GetInt());
	}

	void SetServerAndName(const String &name, const String &ip, int port) {
		try_ = true;
		wait_ = 0;
		name_ = name;
		server_.first_ = ip;
		server_.second_ = port;
	}

	void Update(float timeStep) {
		if (connectedTo_ != server_ && wait_ <= 0 && try_) {
			VariantMap identity_;
			identity_[PLAYER_ID] = playerId_;
			identity_[PLAYER_NAME] = name_;
			if (GetSubsystem<Network>()->Connect(server_.first_, server_.second_, GetScene(), identity_)) {
				connectedTo_ = server_;
			}
		} else {
			wait_ -= timeStep;
		}
	}

	void HandleDisonnect(StringHash eventType, VariantMap &eventData) {
		URHO3D_LOGWARNING("ConnectionKeeper: "+String{eventType.Value()});
		if (eventType == E_CONNECTFAILED)
			try_ = false;
		wait_ = 1;
	}

	const String& GetPlayerId() const {
		return playerId_;
	}

private:
	bool try_ = true;
	float wait_ = -1;
	Network *network_;
	const String playerId_ = String { Random() } + String { Random() };
	String name_;
	Pair<String, int> server_ { "none", 0 };
	Pair<String, int> connectedTo_ { "none", 0 };
	Connection *connection_;
};

#endif /* SOURCE_SRC_CLIENT_CONNECTION_KEEPER_H_ */
