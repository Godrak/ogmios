/*
 * param_vars.h
 *
 *  Created on: Sep 28, 2020
 *      Author: Pavel Mikus
 *		email: pavel.mikus.mail@seznam.cz
 */

#ifndef SOURCE_SRC_PARAM_VARS_H_
#define SOURCE_SRC_PARAM_VARS_H_

#include "jparam_structs.h"

namespace param {

PARAMWD(DampingForce, float, 0.2f, "players movement damping");
PARAMWD(MoveImpulse, float, 1.0f, "players movement impulse strength");
PARAMWD(WalkAnimSpeed, float, 2.3f, "walking animation speed");
PARAMWD(CameraPosition, Vector3, Vector3(0.0f, 35.0f, -14.0f), "position of camera");

PARAMWD(PlaneScale, Vector3, Vector3(33.0f, 1.0f, 33.0f), "scale of plane");

PARAMWD(Wall0Pos, Vector3, Vector3(0.0f, 3.0f, 33.0f / 2), "position of wall 0");
PARAMWD(Wall1Pos, Vector3, Vector3(0.0f, 3.0f, -33.0f / 2), "position of wall 1");
PARAMWD(Wall2Pos, Vector3, Vector3(33.0f / 2, 3.0f, 0.0f), "position of wall 2");
PARAMWD(Wall3Pos, Vector3, Vector3(-33.0f / 2, 3.0f, 0.0f), "position of wall 3");

PARAMWD(Wall0Scale, Vector3, Vector3(33.0f, 3.0f, 3.0f), "scale of wall 0");
PARAMWD(Wall1Scale, Vector3, Vector3(33.0f, 3.0f, 3.0f), "scale of wall 1");
PARAMWD(Wall2Scale, Vector3, Vector3(3.0f, 3.0f, 33.0f), "scale of wall 2");
PARAMWD(Wall3Scale, Vector3, Vector3(3.0f, 3.0f, 33.0f), "scale of wall 3");

}

#endif /* SOURCE_SRC_PARAM_VARS_H_ */
