/*
 * events.h
 *
 *  Created on: Sep 18, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_EVENTS_H_
#define SOURCE_SRC_EVENTS_H_

#include "includeUrho.h"

//pacifist, chicken rights activist, dinner, target practice, pecking queen, ghost, rambo,

static constexpr int FONT_SIZE = 14;

const StringHash PLAYER_ID { "playerId" };
const StringHash PLAYER_NAME { "playerName" };

URHO3D_EVENT(E_PRESSED_KEYS, pressedKeys) {
URHO3D_PARAM(KEYS, keys);
URHO3D_PARAM(PLAYERID, playerId);
}

#endif /* SOURCE_SRC_EVENTS_H_ */
