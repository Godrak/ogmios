/*
 * thread.h
 *
 *  Created on: Sep 5, 2020
 *      Author: Pavel Mikus
 *		email: pavel.mikus.mail@seznam.cz
 */

#ifndef SOURCE_SRC_THREAD_H_
#define SOURCE_SRC_THREAD_H_

#include "includeUrho.h"
#include "Urho3D/Core/Thread.h"

template<typename T>
class ProcessingThread: public Thread {
public:
	ProcessingThread(T *master) :
			Thread(), master_(master) {
	}

	virtual void ThreadFunction() override {
		master_->RunWorkInThread();
	}

	T *master_;
};

#endif /* SOURCE_SRC_THREAD_H_ */
