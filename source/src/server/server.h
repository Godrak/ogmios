#pragma once

#include "../hencoop.h"

using namespace Urho3D;

class Server: public Hencoop {
URHO3D_OBJECT(Server, Hencoop)
public:
	explicit Server(Context *context);
	virtual void Start() override;
protected:

	void InitScene();
	void InitLocal();

private:
	void HandleKeyDown(StringHash eventType, VariantMap &eventData);
	void HandleConnection(StringHash eventType, VariantMap &eventData);

private:
	Scene *scene_;
};

URHO3D_DEFINE_APPLICATION_MAIN(Server);
