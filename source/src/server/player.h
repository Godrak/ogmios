/*
 * player.h
 *
 *  Created on: Sep 25, 2020
 *      Author: Pavel Mikus
 *		email: pavel.mikus.mail@seznam.cz
 */

#ifndef SOURCE_SRC_PLAYER_H_
#define SOURCE_SRC_PLAYER_H_

#include "../includeUrho.h"
#include "../events.h"
#include "map.h"
#include "unit.h"
#include "../custom_key_map.h"

class Player: public LogicComponent {
URHO3D_OBJECT(Player, LogicComponent)
public:
	explicit Player(Context* context) : LogicComponent(context) {
		SubscribeToEvent(E_PRESSED_KEYS, URHO3D_HANDLER(Player, HandlePressedKeys));
	}

	void SetConnection(Connection* connection) {
		auto identity = connection->GetIdentity();
		playerId_ = identity[PLAYER_ID].GetString();
		playerName_ = identity[PLAYER_NAME].GetString();
		connection_ = connection;
		GetNode()->SetOwner(connection);
	}

	void Start() override {
		auto unit = GetNode()->CreateComponent<Unit>(CreateMode::LOCAL);
	}

	void Update(float timeStep) {
		GetComponent<Unit>()->Move(moveVector_);
	}

	void HandlePressedKeys(StringHash eventType, VariantMap& eventData) {
		if (eventData[pressedKeys::PLAYERID].GetString() != playerId_) {
			return;
		}

		auto keys = eventData[pressedKeys::KEYS].GetBuffer();
		Vector3 moveVector { };
		if (keys.Contains(OKEY_W) || keys.Contains(OKEY_UP)) {
			moveVector = Vector3::FORWARD;
		}
		if (keys.Contains(OKEY_A) || keys.Contains(OKEY_LEFT)) {
			moveVector = Vector3::LEFT;
		}
		if (keys.Contains(OKEY_S) || keys.Contains(OKEY_DOWN)) {
			moveVector = Vector3::BACK;
		}
		if (keys.Contains(OKEY_D) || keys.Contains(OKEY_RIGHT)) {
			moveVector = Vector3::RIGHT;
		}
		moveVector_ = moveVector;
		if (keys.Contains(OKEY_SPACE)) {
			GetComponent<Unit>()->Fire();
		}
	}

	Vector3 moveVector_ { };

	String playerId_;
	String playerName_;
	Connection* connection_;
};

#endif /* SOURCE_SRC_PLAYER_H_ */
