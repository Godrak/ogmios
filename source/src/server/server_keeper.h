/*
 * server_keeper.h
 *
 *  Created on: Sep 13, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_SERVER_SERVER_KEEPER_H_
#define SOURCE_SRC_SERVER_SERVER_KEEPER_H_

#include "../includeUrho.h"
#include "player.h"

class ServerKeeper: public LogicComponent {
URHO3D_OBJECT(ServerKeeper, LogicComponent)
public:
	explicit ServerKeeper(Context *context) :
			LogicComponent(context) {
		SubscribeToEvent(E_CLIENTIDENTITY, URHO3D_HANDLER(ServerKeeper, HandleConnection));
	}

	void Start() override {
		auto *network = GetSubsystem<Network>();
		network->RegisterRemoteEvent(E_PRESSED_KEYS);
		network->StartServer(1234, 16);
	}

	void HandleConnection(StringHash eventType, VariantMap &eventData) {
		using namespace ClientConnected;
		auto *network = GetSubsystem<Network>();
		auto *connection = static_cast<Connection*>(eventData[P_CONNECTION].GetPtr());
		connection->SetScene(GetScene());

		auto playerId = connection->GetIdentity()[PLAYER_ID].GetString();
		Node *playerNode = GetScene()->GetChild(playerId, true);
		if (playerNode != nullptr) {
			playerNode->GetComponent<Player>()->SetConnection(connection);
		} else {
			Node *playerNode = GetScene()->CreateChild("player");
			auto *player = playerNode->CreateComponent<Player>(CreateMode::LOCAL);
			player->SetConnection(connection);
		}
		connections_.Populate(playerId, connection);
	}

	HashMap<String, Connection*> connections_;
};

#endif /* SOURCE_SRC_SERVER_SERVER_KEEPER_H_ */
