/*
 * cannon.h
 *
 *  Created on: Oct 12, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_SERVER_CANNON_H
#define SOURCE_SRC_SERVER_CANNON_H

#include "../includeUrho.h"
#include "../events.h"
#include "projectile.h"

class Cannon: public LogicComponent {
URHO3D_OBJECT(Cannon, LogicComponent)
public:
	explicit Cannon(Context *context) :
			LogicComponent(context) {
	}

	void Start() override {
		graphicsNode_ = GetNode()->CreateChild();
		graphicsNode_->SetScale(0.015);
		graphicsNode_->SetPosition(Vector3 { 0.8, 1, 0.3 });
		Quaternion q;
		q.FromAngleAxis(180, Vector3::UP);
		graphicsNode_->SetRotation(q);
		auto cache = GetSubsystem<ResourceCache>();
		model_ = graphicsNode_->CreateComponent<StaticModel>();
		model_->SetModel(cache->GetResource<Model>(modelPath_));
		model_->SetMaterial(cache->GetResource<Material>(materialPath_));
		model_->SetCastShadows(true);

	}

	void Fire() {
		if (cooldown_ > 0) {
			return;
		}
		cooldown_ = 0.5;
		auto projectileNode = GetScene()->CreateTemporaryChild();
		auto projectile = projectileNode->CreateComponent<Projectile>(CreateMode::LOCAL);
		projectile->Fire(graphicsNode_->GetWorldPosition() - 1.5 * graphicsNode_->GetWorldDirection() + Vector3::UP,
				-graphicsNode_->GetWorldDirection());
	}

	void Update(float timeStep) {
		cooldown_ -= timeStep;
	}

	void Handle(StringHash eventType, VariantMap &eventData) {
	}

protected:
	float cooldown_ = 0.5;

	Node *graphicsNode_;
	StaticModel *model_;

	String modelPath_ = "Models/Cannon/Cannon.mdl";
	String materialPath_ = "Materials/black.xml";
};

#endif /* SOURCE_SRC_SERVER_CANNON_H */
