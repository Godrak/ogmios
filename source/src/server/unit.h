/*
 * unit.h
 *
 *  Created on: Sep 24, 2020
 *      Author: Pavel Mikus
 *		email: pavel.mikus.mail@seznam.cz
 */

#ifndef SOURCE_SRC_UNIT_H_
#define SOURCE_SRC_UNIT_H_

#include "../includeUrho.h"
#include "../hencoop.h"
#include "../events.h"
#include "chicken.h"
#include "cannon.h"
#include "map.h"

class Unit: public LogicComponent {
URHO3D_OBJECT(Unit, LogicComponent)
public:
	explicit Unit(Context *context) :
			LogicComponent(context) {
	}

	void Start() override {
		rigidBody_ = GetNode()->CreateComponent<RigidBody>(CreateMode::LOCAL);
		rigidBody_->SetMass(1);
		rigidBody_->SetCollisionLayer(1);
		rigidBody_->SetAngularFactor(Vector3::ZERO);

		auto *collisionShape = GetNode()->CreateComponent<CollisionShape>(CreateMode::LOCAL);
		collisionShape->SetCapsule(2, 1.8, Vector3(0, 0.9, 0));

		GetNode()->CreateComponent<Chicken>(CreateMode::LOCAL);
		GetNode()->CreateComponent<Cannon>(CreateMode::LOCAL);

		SubscribeToEvent(GetNode(), E_NODECOLLISION, URHO3D_HANDLER(Unit, HandleNodeCollision));
		Reset();
	}

	void Reset() {
		auto pos = GetScene()->GetComponent<Map>()->GetSpawnPosition();
		GetNode()->SetPosition(pos);
		alive_ = true;
		resetTime_ = 2;
		rigidBody_->Activate();
	}

	void Update(float timeStep) {
		if (alive_) {
			const Vector3 &velocity = rigidBody_->GetLinearVelocity();
			// Velocity on the XZ plane
			Vector3 planeVelocity(velocity.x_, 0.0f, velocity.z_);
			rigidBody_->ApplyImpulse(-planeVelocity * GetParam(param::DampingForce));

			if (!BoundingBox { -100, 100 }.IsInside(GetNode()->GetWorldPosition())) {
				Die();
			}
		} else if (resetTime_ < 0) {
			Reset();
		} else {
			resetTime_ -= timeStep;
		}
	}

	void Move(const Vector3 &moveVector) {
		if (alive_) {
			if (moveVector.LengthSquared() > 0) {
				GetNode()->SetDirection(moveVector);
				rigidBody_->ApplyImpulse(moveVector * GetParam(param::MoveImpulse));
				GetComponent<Chicken>()->Walk();
			} else {
				GetComponent<Chicken>()->StopWalking();
			}
		}
	}

	void Fire() {
		GetComponent<Cannon>()->Fire();
	}

	void Die() {
		URHO3D_LOGDEBUG("dying");
		alive_ = false;
		GetComponent<Chicken>()->Die();
	}

	void HandleNodeCollision(StringHash eventType, VariantMap &eventData) {
		Node *otherNode = (Node*) eventData[NodeCollision::P_OTHERNODE].GetPtr();
		if (otherNode != nullptr && otherNode->GetComponent<Projectile>() != nullptr) {
			Die();
		}
	}

	RigidBody *rigidBody_;
	bool alive_ = true;
	float resetTime_ = 2;
};

#endif /* SOURCE_SRC_UNIT_H_ */
