#include "server.h"

#include "chicken.h"
#include "server_keeper.h"
#include "unit.h"
#include "player.h"
#include "map.h"
#include "cannon.h"

Server::Server(Context* context) : Hencoop(context) {
	SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(Server, HandleKeyDown));

	context->RegisterFactory<ServerKeeper>();
	context->RegisterFactory<Unit>();
	context->RegisterFactory<Chicken>();
	context->RegisterFactory<Player>();
	context->RegisterFactory<Map>();
	context->RegisterFactory<Cannon>();
	context->RegisterFactory<Projectile>();
}

void Server::Start() {
	Hencoop::Start();
	InitScene();
	InitLocal();
}

void Server::InitScene() {
	ResourceCache* cache = GetSubsystem<ResourceCache>();
	scene_ = new Scene(context_);

	// Create the Octree component to the scene. This is required before adding any drawable components, or else nothing will
	// show up. The default octree volume will be from (-1000, -1000, -1000) to (1000, 1000, 1000) in world coordinates; it
	// is also legal to place objects outside the volume but their visibility can then not be checked in a hierarchically
	// optimizing manner
	scene_->CreateComponent<Octree>();
	auto physics = scene_->CreateComponent<PhysicsWorld>(CreateMode::LOCAL);
	physics->SetInterpolation(false);

	scene_->CreateComponent<Map>(CreateMode::LOCAL);
}

void Server::InitLocal() {
	scene_->CreateComponent<CameraController>(CreateMode::LOCAL);
	scene_->CreateComponent<ServerKeeper>(CreateMode::LOCAL);
}

void Server::HandleKeyDown(StringHash eventType, VariantMap& eventData) {
	using namespace KeyDown;

	auto* input = GetSubsystem<Input>();
	if (input->GetKeyDown(KEY_ESCAPE)) {
		engine_->Exit();
	}
}
