/*
 * map.h
 *
 *  Created on: Sep 25, 2020
 *      Author: Pavel Mikus
 *		email: pavel.mikus.mail@seznam.cz
 */

#ifndef SOURCE_SRC_MAP_H_
#define SOURCE_SRC_MAP_H_

#include "../includeUrho.h"
#include "../events.h"
#include "../params.h"
#include "../param_vars.h"

class Map: public LogicComponent {
URHO3D_OBJECT(Map, LogicComponent)
public:
	explicit Map(Context *context) :
			LogicComponent(context) {
	}

	void Start() override {
		auto cache = GetSubsystem<ResourceCache>();
		// Create a child scene node (at world origin) and a StaticModel component into it. Set the StaticModel to show a simple
		// plane mesh with a "stone" material. Note that naming the scene nodes is optional. Scale the scene node larger
		// (100 x 100 world units)

		Node *planeNode = GetScene()->CreateChild("Plane");
		planeNode->SetScale(GetParam(param::PlaneScale));
		auto *planeObject = planeNode->CreateComponent<StaticModel>();
		planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
		planeObject->SetMaterial(cache->GetResource<Material>("Models/brown.xml"));

		planeNode->CreateComponent<RigidBody>();
		auto *shape = planeNode->CreateComponent<CollisionShape>();
		shape->SetBox(Vector3(1, 1, 1));

		CreateWall(GetParam(param::Wall0Pos), GetParam(param::Wall0Scale));
		CreateWall(GetParam(param::Wall1Pos), GetParam(param::Wall1Scale));
		CreateWall(GetParam(param::Wall2Pos), GetParam(param::Wall2Scale));
		CreateWall(GetParam(param::Wall3Pos), GetParam(param::Wall3Scale));

		Node *object = GetScene()->CreateChild("object");
		object->SetScale(6);
		object->SetPosition(Vector3 { 0, 3, 0 });
		auto objectModel = object->CreateComponent<StaticModel>();
		objectModel->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
		objectModel->SetMaterial(cache->GetResource<Material>("Models/almost_black.xml"));
		object->CreateComponent<RigidBody>();
		auto *objectShape = object->CreateComponent<CollisionShape>();
		objectShape->SetTriangleMesh(objectModel->GetModel());

		Node *zoneNode = GetScene()->CreateChild("Zone");
		auto *zone = zoneNode->CreateComponent<Zone>();
		zone->SetAmbientColor(Color(0.20f, 0.20f, 0.20f));
		zone->SetFogColor(Color(0.6f, 0.6f, 0.6f));
		zone->SetFogStart(300.0f);
		zone->SetFogEnd(500.0f);
		zone->SetBoundingBox(BoundingBox(-2000.0f, 2000.0f));

		// Create a directional light with cascaded shadow mapping
		Node *lightNode = GetScene()->CreateChild("DirectionalLight");
		lightNode->SetDirection(Vector3(0.02f, -0.8f, 0.3f));
		auto *light = lightNode->CreateComponent<Light>();
		light->SetLightType(LIGHT_DIRECTIONAL);
		light->SetCastShadows(true);
		light->SetShadowBias(BiasParameters(0.00025f, 0.5f));
		light->SetShadowCascade(CascadeParameters(10.0f, 50.0f, 200.0f, 0.0f, 0.8f));
		light->SetSpecularIntensity(0.5f);
		light->SetShadowIntensity(0.5f);
	}

	Vector3 GetSpawnPosition() {
		if (Random() < 0.5) {
			return Vector3 { -13 * (Random() > 0.5 ? -1 : 1), 1, -13 + Random() * 26 };
		} else {
			return Vector3 { -13 + Random() * 26, 1, -13 * (Random() > 0.5 ? -1 : 1) };
		}
	}

	void Update(float timeStep) {
	}

	void Handle(StringHash eventType, VariantMap &eventData) {
	}

	Node* CreateWall(const Vector3 &position, const Vector3 &scale) {
		auto cache = GetSubsystem<ResourceCache>();
		Node *wall = GetScene()->CreateChild("box");
		wall->SetScale(scale);
		wall->SetPosition(position);
		auto model = wall->CreateComponent<StaticModel>();
		model->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
		model->SetMaterial(cache->GetResource<Material>("Models/almost_black.xml"));
		wall->CreateComponent<RigidBody>();
		auto *shape = wall->CreateComponent<CollisionShape>();
		shape->SetBox(Vector3(1, 1, 1));
		return wall;
	}

};

#endif /* SOURCE_SRC_MAP_H_ */
