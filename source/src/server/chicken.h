/*
 * chicken.h
 *
 *  Created on: Sep 24, 2020
 *      Author: Pavel Mikus
 *		email: pavel.mikus.mail@seznam.cz
 */

#ifndef SOURCE_SRC_CHICKEN_H_
#define SOURCE_SRC_CHICKEN_H_

#include "../includeUrho.h"
#include "../events.h"

class Chicken: public LogicComponent {
URHO3D_OBJECT(Chicken, LogicComponent)
public:
	explicit Chicken(Context *context) :
			LogicComponent(context) {
	}

	void Start() override {
		auto graphicsNode = GetNode()->CreateChild();
		graphicsNode->SetScale(0.06);
		auto cache = GetSubsystem<ResourceCache>();
		animatedModel_ = graphicsNode->CreateComponent<AnimatedModel>();
		animatedModel_->SetModel(cache->GetResource<Model>(modelPath_));
		animatedModel_->SetMaterial(cache->GetResource<Material>(materialPath_));
		animatedModel_->SetCastShadows(true);
		animationController_ = graphicsNode->CreateComponent<AnimationController>();

	}

	void Walk() {
		animationController_->PlayExclusive(walkAnimPath_, 0, true, 0.0f);
		animationController_->SetSpeed(walkAnimPath_, GetParam(param::WalkAnimSpeed));
	}

	void StopWalking() {
		animationController_->PlayExclusive(standAnimPath_, 0, true, 0.3f);
	}

	void Die() {
		animationController_->PlayExclusive(dyingAnimPath_, 0, false, 0.0f);
	}

	void Update(float timeStep) {
	}

	void Handle(StringHash eventType, VariantMap &eventData) {
	}

protected:
	AnimatedModel *animatedModel_;
	AnimationController *animationController_;

	String modelPath_ = "Models/Chicken/Chicken.mdl";
	String materialPath_ = "Models/Chicken/Materials/Chicken.xml";

	String walkAnimPath_ = "Models/Chicken/Chicken_ArmatureWalk.ani";
	String dyingAnimPath_ = "Models/Chicken/Chicken_ArmatureDying.ani";
	String standAnimPath_ = "Models/Chicken/Chicken_ArmatureStand.ani";
	String idle0AnimPath_ = "Models/Chicken/Chicken_ArmatureIdle.ani";
	String idle1AnimPath_ = "Models/Chicken/Chicken_ArmatureIdle1.ani";
	String idle2AnimPath_ = "Models/Chicken/Chicken_ArmatureIdle2.ani";

};

#endif /* SOURCE_SRC_CHICKEN_H_ */
