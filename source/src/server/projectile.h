/*
 * projectile.h
 *
 *  Created on: Oct 12, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_SERVER_PROJECTILE_H_
#define SOURCE_SRC_SERVER_PROJECTILE_H_

#include "../includeUrho.h"
#include "../events.h"

class Projectile: public LogicComponent {
URHO3D_OBJECT(Projectile, LogicComponent)
public:
	Projectile(Context *context) :
			LogicComponent(context) {
	}

	void Fire(Vector3 position, Vector3 direction) {
		GetNode()->SetWorldPosition(position);
		GetNode()->SetScale(0.003);
		direction.Normalize();
		direction_ = direction;
		GetNode()->SetDirection(-direction);

		auto cache = GetSubsystem<ResourceCache>();
		auto *staticModel = GetNode()->CreateComponent<StaticModel>();
		auto *model = cache->GetResource<Model>(modelPath_);
		auto *material = cache->GetResource<Material>(materialPath_);
		staticModel->SetModel(model);
		staticModel->SetMaterial(material);
		GetNode()->CreateComponent<RigidBody>();

		auto *collisionShape = GetNode()->CreateComponent<CollisionShape>();
		collisionShape->SetShapeType(SHAPE_SPHERE);
		collisionShape->SetSize(Vector3::ONE * 300);

		SubscribeToEvent(GetNode(), E_NODECOLLISION, URHO3D_HANDLER(Projectile, HandleNodeCollision));
	}

	void HandleNodeCollision(StringHash eventType, VariantMap &eventData) {
		GetNode()->Remove();
	}

	void Update(float timeStep) override {
		GetNode()->SetPosition(GetNode()->GetPosition() + timeStep * speed_ * direction_);

		if (!BoundingBox { -100, 100 }.IsInside(GetNode()->GetWorldPosition())) {
			GetNode()->Remove();
		}
	}

protected:
	Vector3 direction_;
	float speed_ = 30;

	String modelPath_ = "Models/Egg/Egg.mdl";
	String materialPath_ = "Models/white.xml";
};

#endif /* SOURCE_SRC_SERVER_PROJECTILE_H_ */
