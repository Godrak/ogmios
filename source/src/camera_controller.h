/*
 * camera_controller.h
 *
 *  Created on: Sep 13, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_CAMERA_CONTROLLER_H_
#define SOURCE_SRC_CAMERA_CONTROLLER_H_

#include "includeUrho.h"
#include "params.h"
#include "param_vars.h"

class CameraController: public LogicComponent {
URHO3D_OBJECT(CameraController, LogicComponent)
public:
	explicit CameraController(Context* context) : LogicComponent(context) {
	}

	void Start() override {
		cameraNode_ = GetNode()->CreateChild("Camera", CreateMode::LOCAL);
		cameraNode_->CreateComponent<Camera>();
		// Set an initial position for the camera scene node above the plane
		cameraNode_->SetPosition(GetParam(param::CameraPosition));
		cameraNode_->SetRotation(Quaternion(70.0f, 0.0f, 0.0f));

		auto* renderer = GetSubsystem<Renderer>();
		// Set up a viewport to the Renderer subsystem so that the 3D scene can be seen. We need to define the scene and the camera
		// at minimum. Additionally we could configure the viewport screen size and the rendering path (eg. forward / deferred) to
		// use, but now we just use full screen and default render path configured in the engine command line options
		SharedPtr<Viewport> viewport(new Viewport(context_, GetScene(), cameraNode_->GetComponent<Camera>()));
		renderer->SetViewport(0, viewport);
	}

private:
	Node* cameraNode_;
};

#endif /* SOURCE_SRC_CAMERA_CONTROLLER_H_ */
