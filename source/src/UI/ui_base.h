/*
 * ui_base.h
 *
 *  Created on: Sep 18, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_UI_UI_BASE_H_
#define SOURCE_SRC_UI_UI_BASE_H_

#include "../includeUrho.h"
#include "../events.h"

Text* CreateText(UIElement *element, String text) {
	auto t = element->CreateChild<Text>("text");
	t->SetStyleAuto();
	t->SetText(text);
	t->SetFontSize(FONT_SIZE);
	return t;
}

template<typename T>
T* CreateFramedText(UIElement *element, String text = "") {
	auto frame = element->CreateChild<T>();
	frame->SetStyleAuto();
	frame->SetLayout(LayoutMode::LM_HORIZONTAL, 0, IntRect { FONT_SIZE / 3, FONT_SIZE / 3, FONT_SIZE / 3, FONT_SIZE / 3 });
	frame->SetMinSize(IntVector2 { FONT_SIZE * text.Length(), FONT_SIZE * 2 });
	if (!text.Empty()) {
		CreateText(frame, text);
	}
	return frame;
}

template<typename T>
T* CreateFramedOption(UIElement *element, String text) {
	auto frame = element->CreateChild<Window>();
	frame->SetStyleAuto();
	frame->SetLayout(LayoutMode::LM_HORIZONTAL, FONT_SIZE, IntRect { FONT_SIZE / 3, FONT_SIZE / 3, FONT_SIZE / 3, FONT_SIZE / 3 });
	frame->SetMinSize(IntVector2 { FONT_SIZE * text.Length(), FONT_SIZE * 2 });
	CreateText(frame, text);
	auto res = frame->CreateChild<T>();
	res->SetStyleAuto();
	return res;
}

class UIBase: public UIElement {
URHO3D_OBJECT(UIBase, UIElement)
	Window *window_ { };
	Button *acceptButton_ { };

	explicit UIBase(Context *context) :
			UIElement(context) {
	}

	void Init() {
		window_ = GetParent()->CreateChild<Window>();
		window_->SetStyleAuto();
		window_->SetLayout(LayoutMode::LM_VERTICAL, 0, IntRect { });
		window_->SetMovable(true);

		auto bar = window_->CreateChild<Window>("bar");
		bar->SetStyleAuto();
		bar->SetMaxHeight(FONT_SIZE * 2);
		bar->SetLayout(LayoutMode::LM_HORIZONTAL, 0, IntRect { });

		auto text = CreateFramedText<Window>(bar, GetWindowName());

		auto remove = CreateFramedText<Button>(bar, "");
		remove->SetLayout(LayoutMode::LM_FREE);
		remove->SetStyle("CloseButton");
		remove->SetHorizontalAlignment(HorizontalAlignment::HA_RIGHT);
		SubscribeToEvent(remove, E_RELEASED, URHO3D_HANDLER(UIBase, HandleRemove));

		bar->SetFixedHeight(bar->GetHeight());
		SetupWindowContent();
		FixLayout();

		Show(false);
	}

	virtual void FixLayout() {
		auto graphics = GetSubsystem<Graphics>();
		auto size = graphics->GetSize();
		window_->SetPosition(size.x_ / 2 + window_->GetWidth() * Random(-1, 1), FONT_SIZE * Random(5, 10));
	}

	virtual void Show(bool value) {
		if (value && window_->IsVisible()) {
			FixLayout();
		} else {
			window_->SetVisible(value);
		}
	}

	void HandleRemove(StringHash eventType, VariantMap &eventData) {
		Show(false);
	}

	virtual String GetWindowName() =0;
	virtual void SetupWindowContent() =0;
};

#endif /* SOURCE_SRC_UI_UI_BASE_H_ */
