/*
 * params.h
 *
 *  Created on: Sep 28, 2020
 *      Author: Pavel Mikus
 *		email: pavel.mikus.mail@seznam.cz
 */

#ifndef SOURCE_SRC_PARAMS_H_
#define SOURCE_SRC_PARAMS_H_

#include "includeUrho.h"
#include "json_params.h"
#include "param_vars.h"

namespace json_params {
template<>
inline Vector3 JsonParamsHandler::getValue(const std::string& key) const {
	bool array = params[key].length() == 3;

	if (array) {
		bool resOk = true;
		bool ok = false;
		float res0 = getFractional(params[key][0], ok);
		resOk = ok && resOk;
		float res1 = getFractional(params[key][1], ok);
		resOk = ok && resOk;
		float res2 = getFractional(params[key][2], ok);
		resOk = ok && resOk;

		if (resOk)
			return Vector3 { res0, res1, res2 };
	}
	//the type of this value is not numeric -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, array of 3 numbers expected" << std::endl;
	exit(1);
}
}

class Params: public Object {
URHO3D_OBJECT(Params, Object)
public:
	explicit Params(Context* context) : Object(context) {
	}

	void Init() {
		auto cache = GetSubsystem<ResourceCache>();
		auto json = cache->GetResource<JSONFile>("params.json");

		handler_ = json_params::JsonParamsHandler { json->ToString().CString(), 0 };
	}

	json_params::JsonParamsHandler handler_ { "", 0 };
};

#define GetParam(name) GetSubsystem<Params>()->handler_.get<name>()

#endif /* SOURCE_SRC_PARAMS_H_ */
