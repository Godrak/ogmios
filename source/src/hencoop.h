/*
 * hencoop.h
 *
 *  Created on: Sep 4, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SOURCE_SRC_HENCOOP_H_
#define SOURCE_SRC_HENCOOP_H_

#include "includeUrho.h"
#include "camera_controller.h"
#include "params.h"
#include <random>

using namespace Urho3D;

class Hencoop: public Application {
URHO3D_OBJECT(Hencoop, Application)
public:
	explicit Hencoop(Context *context) :
			Application(context) {
		context->RegisterFactory<CameraController>();
	}

	void Setup() override {
		std::random_device rd;
		SetRandomSeed(rd());
		// Modify engine startup parameters
		engineParameters_[EP_WINDOW_TITLE] = GetTypeName();
		engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("urho3d", "logs") + GetTypeName() + ".log";
		engineParameters_[EP_FULL_SCREEN] = false;
		engineParameters_[EP_HEADLESS] = false;

		// Construct a search path to find the resource prefix with two entries:
		// The first entry is an empty path which will be substituted with program/bin directory -- this entry is for binary when it is still in build tree
		// The second and third entries are possible relative paths from the installed program/bin directory to the asset directory -- these entries are for binary when it is in the Urho3D SDK installation location
		if (!engineParameters_.Contains(EP_RESOURCE_PREFIX_PATHS))
			engineParameters_[EP_RESOURCE_PREFIX_PATHS] = ";../share/Resources;../share/Urho3D/Resources";
	}

	void Start() override {
		GetSubsystem<Input>()->SetMouseMode(MM_FREE);
		GetSubsystem<Input>()->SetMouseVisible(true);

		GetContext()->RegisterSubsystem(new Params { GetContext() });
		GetSubsystem<Params>()->Init();
	}

};

#endif /* SOURCE_SRC_HENCOOP_H_ */
